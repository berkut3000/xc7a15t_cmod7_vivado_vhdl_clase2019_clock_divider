
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity TB_FFCLK is
end TB_FFCLK;

architecture Behavioral of TB_FFCLK is

begin


end Behavioral;



 
ARCHITECTURE behavior OF TB_FFCLK IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT FLIPFLOP is
        port(
                CLK, CE, D: in  std_logic;    
                Q: out std_logic
              );    
    END COMPONENT;
    

   --Inputs
   signal CLK, CE, D : std_logic := '0';

 	--Outputs
   signal Q : std_logic := '0';

   -- Clock period definitions
   constant CLK_period : time := 83.333 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: FLIPFLOP PORT MAP (
          CLK => CLK,
          CE => CE,
          D => D,
          Q => Q
        );

   -- Clock process definitions
   CLK_process :process
   begin
		CLK <= '0';
		wait for CLK_period/2;
		CLK <= '1';
		wait for CLK_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 55 us;	
      -- insert stimulus here 
      D <= '1';  
      CE <= '1';
      wait for 20 ns;	
      D <= '0';  
      CE <= '0';
      wait for 20 ns;	
      D <= '1';  
      CE <= '0';
      wait for 20 ns;	
      D <= '0';  
      CE <= '0';
      wait for 20 ns;	
      D <= '1';  
      CE <= '0';
      wait;
      wait for 20 ns;	
      D <= '0';  
      CE <= '0';
      wait for 20 ns;	
      D <= '1';  
      CE <= '0';
      wait for 20 ns;	
      D <= '1';  
      CE <= '0';
      wait for 20 ns;	
      D <= '0';  
      CE <= '1';
      wait;
   end process;

END;
