----------------------------------------------------------------------------------
-- JORGE SOTO
-- Programa para demostrar el uso de FlipFlops del FPGA
----------------------------------------------------------------------------------
-- Librerias:
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity FLIPFLOP is
	port(
			CLK, CE, D: in  std_logic;	
			Q: out std_logic
		  );	
	attribute IOB : string;
   attribute IOB of Q : signal is "true"; 
-- true usa FF salida, false usa FF slice
-- sin atributo donde quiera
end FLIPFLOP;

architecture Behavioral of FLIPFLOP is

component clk_wiz_0
port
 (-- Clock in ports
  -- Clock out ports
  clk_out1          : out    std_logic;
  -- Status and control signals
  reset             : in     std_logic;
  locked            : out    std_logic;
  clk_in1           : in     std_logic
 );
end component;

signal reset : std_logic := '0';
signal locked : std_logic;
signal CLKO : std_logic;

begin

RELOJ : clk_wiz_0
   port map ( 
  -- Clock out ports  
   clk_out1 => CLKO,
  -- Status and control signals                
   reset => reset,
   locked => locked,
   -- Clock in ports
   clk_in1 => CLK
 );

	process(CLKO) begin
		if rising_edge(CLKO) then
			if CE = '0' then
				Q <= D;
			else
				Q <= '0';
			end if;
		end if;
	end process;

end Behavioral;

